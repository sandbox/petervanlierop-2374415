# README #

### What is this repository for? ###

* Quick summary
  This module facilitates the exposure of content between two websites in a
  secure way. This module must be installed on both websites. The website that
  stores the external content must have a user with an OAUTH consumer and the
  appropriate permissions. The module includes an administration interface for
  host, public key and secret. This is stored on the website that exposes the
  content and it is used for authorisation purpose.
  When you use a link to external content in your own content the relative path
  will be replaced by the absolute path on saving. The link to the external
  content facilitates the use of a placeholder like this:
  [external-embed:remote_site.com/content/nice_article|my_placeholder].
  When you don't use a place holder the pattern looks like this:
  [external-embed:remote_site.com/content/nice_article]. The square brackets, 
  the text: 'external-embed' and the colon are mandatory. Characters after a
  pipe are considered to be a placeholder.

  There is also support to copy the right link:
  A tab on the content page, titled 'Attachments', shows the link, ready for use.

  
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
  Install and enable the module on all involved sites.
  Create users on the sites that store the external contents with the
  appropriate permissions.
  Create OAUTH consumers for these users.
  On the exposing website go to:
  "Configuration/Web services/Access Key Configuration" and instruct: the
  external host, the public key and the secret of the OAUTH consumer.  

* Configuration
* Dependencies
    ctools
    features
    oauth_common
	entity
	services
	services_oauth
	rest_server
* Database configuration
    Schema: external_embed_remote_access
      field: host (unique)
      field: public_key
      field: secret
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact
