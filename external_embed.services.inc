<?php
/**
 * @file
 * Includes Services for the external_embed module.
 */

/**
 * Implements hook_default_services_endpoint().
 */
function external_embed_default_services_endpoint() {
  $export = array();

  $endpoint = new stdClass();
  $endpoint->disabled = FALSE; /* Edit this to true to make a default endpoint disabled initially */
  $endpoint->api_version = 3;
  $endpoint->name = 'external_embed';
  $endpoint->server = 'rest_server';
  $endpoint->path = 'api';
  $endpoint->authentication = array(
    'services_oauth' => array(
      'oauth_context' => 'external_embed',
      'credentials' => 'consumer',
    ),
  );
  $endpoint->server_settings = array();
  $endpoint->resources = array(
    'external-embed' => array(
      'operations' => array(
        'retrieve' => array(
          'enabled' => '1',
          'settings' => array(
            'services_oauth' => array(
              'credentials' => 'consumer',
              'authorization' => 'external_embed',
            ),
          ),
        ),
      ),
    ),
  );
  $endpoint->debug = 0;
  $export['external_embed'] = $endpoint;

  return $export;
}
