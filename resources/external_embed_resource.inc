<?php
/**
 * @file
 * Code for the External embed feature.
 *
 * Retrieve and access services resource.
 */

/**
 * Retrieve services resource.
 */
function _external_embed_resource_retrieve($entity_type, $id) {
  if ($entity_type == 'file') {
    $file = file_load($id);
    $headers = file_download_headers($file->uri);
    if (count($headers)) {
      if (!in_array($file->filemime, array('application/pdf'))) {
        $headers['ext-filename'] = $file->filename;
      }

      file_transfer($file->uri, $headers);
    }
  }
  else {
    $entities = entity_load($entity_type, array($id));
    $entity = end($entities);
    $entity_prepared = entity_view($entity_type, array($entity), 'external_embed');

    return render($entity_prepared);
  }
}

/**
 * Access services resource.
 */
function _external_embed_resource_access($entity_type, $id) {
  $entities = entity_load($entity_type, array($id));
  $entity = end($entities);

  if (entity_access('view', $entity_type, $entity)) {
    return TRUE;
  }
}
